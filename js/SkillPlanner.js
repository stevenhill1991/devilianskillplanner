var SkillPlanner = (function(){
	var app = this;

	app.selectedClass = 'berserker';	
	app.selectedSpec = '';

	app.pointsSpent = 0;
	app.pointsAvailable = 43;

	app.currentBuild = [
		0, [
			[0, [0, 0]],
			[0, [0, 0]],
			[0, [0, 0, 0]],
			[0, [0, 0, 0]],
			[0, [0, 0, 0, 0]]
		],
		[
			[0, [0, 0]],
			[0, [0, 0]],
			[0, [0, 0, 0]],
			[0, [0, 0, 0]],
			[0, [0, 0, 0, 0]]
		],
		[
			[0, [0, 0]],
			[0, [0, 0]],
			[0, [0, 0, 0]],
			[0, [0, 0, 0]],
			[0, [0, 0, 0, 0]]
		]
	];
    
    app.saveTime;
	app.canvas;
	

	

	app.flattenArr = function(arr) {		
	  return arr.reduce(function (flat, toFlatten) {
	    return flat.concat(Array.isArray(toFlatten) ? flattenArr(toFlatten) : toFlatten);
	  }, []);
	}
	app.sumArr = function(arr){
		return arr.reduce(function(pv, cv){
			return pv + cv;
		},0);
	}

	app.getSpecIndex = function(){
		return app.skillData[app.selectedClass].builds.indexOf(app.selectedSpec);
	}

	app.getPointsUsed = function(){
		var usedPoints = 0;
		for(var i = 1; i < currentBuild.length;i++){
			usedPoints+=sumArr(flattenArr(app.currentBuild[i]));
		}
		return usedPoints;
	}

	app.getPointsRemaining = function(){
		return app.pointsAvailable - app.getPointsUsed();
	}
	app.getCurrentSpecPoints = function(){		
		return sumArr(flattenArr(app.currentBuild[app.getSpecIndex()+1]));
	}
	
	var TreeCanvas = function(){
		this.canvasWidth = 700;
		this.canvasHeight = 500;
		this.canvasPadding = 15;

		this.canvasWidthPadded = this.canvasWidth + (this.canvasPadding*2);
		this.canvasHeightPadded = this.canvasHeight + (this.canvasPadding*2);
		
		this.progression = [0, 1, 3, 5, 7, 9, 11, 14, 17, 20];

		this.canvas = $('#tree-canvas');
		this.ctx = this.canvas[0].getContext('2d');
		this.canvas.attr({
			width: this.canvasWidthPadded,
			height: this.canvasHeightPadded,
		});

		this.imageCache = {};

		this.drawLine = function(x1,y1,x2,y2,color){
			this.ctx.closePath();
		    this.ctx.beginPath();
		    this.ctx.strokeStyle = color;
		    this.ctx.moveTo(x1, y1);
		    this.ctx.lineTo(x2, y2);
		    this.ctx.stroke();	
		}

		this.drawTreeLines = function(){
			var step = this.canvasWidth/10;
			for (var x = step; x <= this.canvasWidthPadded - step; x += step) {
			    var xPad = x + this.canvasPadding;
			    var yPad = this.canvasHeight + this.canvasPadding;
			    this.drawLine(xPad--,this.canvasPadding,xPad--,yPad,"#222222");     
			    this.drawLine(xPad,this.canvasPadding,xPad,yPad,"black");    
			    this.drawLine(xPad++,this.canvasPadding,xPad++,yPad,"#222222");      
  			}
		}

		this.drawProgressBar = function(progress){
		  var index = this.progression.indexOf(progress);		 
		  if (index > -1) {
		    var progressPoint = (this.canvasWidth / 10) * (index + 1) + this.canvasPadding;
		   
		    this.ctx.closePath();
		    this.ctx.beginPath();
		    this.ctx.fillStyle = '#D99219';
		    this.ctx.fillRect(0, this.canvasPadding + 0, progressPoint, 5);
		    
		    if (index < this.progression.length) {
		      var endPoint = this.canvasWidthPadded - progressPoint;
		      // draw rest of line in dark color.
		      this.ctx.closePath();
		      this.ctx.beginPath();
		      this.ctx.fillStyle = 'red' //'#38312B';
		      this.ctx.fillRect(progressPoint, this.canvasPadding, endPoint, 5);
                
		    }
		  }

		}

		this.drawProgressMarkers = function(progress) {
		  var index = this.progression.indexOf(progress);
		  
		  if (index > -1) {
		      // fill progress orange   
		      this.ctx.font = "12px  Arial";     
		      this.ctx.textAlign = "center";
		      for(var i = index;i>-1;i--){		       	
		        var progressPoint = (this.canvasWidth / 10) * (i + 1) + this.canvasPadding;
		        this.ctx.closePath();
		        this.ctx.beginPath();         
		        this.ctx.fillStyle = '#5D4923'
		        this.ctx.moveTo(progressPoint-10, this.canvasPadding+0);
		        this.ctx.lineTo(progressPoint+10, this.canvasPadding+0);
		        this.ctx.lineTo(progressPoint, this.canvasPadding+20);
		        this.ctx.fill();
		        this.ctx.fillStyle = "white";
		        this.ctx.fillText(this.progression[i], progressPoint, this.canvasPadding+10);		         
		      }
		      //fill to gain red      
		      if (index+1 < this.progression.length) {
		        
		        for(var i = index+1; i<this.progression.length;i++){
		          var progressPoint = (this.canvasWidth / 10) * (i + 1) + this.canvasPadding;
		          this.ctx.closePath();
		          this.ctx.beginPath();
		          this.ctx.fillStyle = '#47471E';
		          this.ctx.moveTo(progressPoint-10, this.canvasPadding);
		          this.ctx.lineTo(progressPoint+10, this.canvasPadding);
		          this.ctx.lineTo(progressPoint, this.canvasPadding+20);
		          this.ctx.fill(); 
		          this.ctx.fillStyle = "grey";
		          this.ctx.fillText(this.progression[i], progressPoint, this.canvasPadding+10);
		        }
		      }
		    
		  }
		}

		this.drawProgressShadow = function(progress){
		  var index = this.progression.indexOf(progress);		 
		  if (index > -1) {
		    var progressPoint = (this.canvasWidth / 10) * (index + 1) +	this.canvasPadding;		  
		    this.ctx.closePath();
		    this.ctx.beginPath();
		    this.ctx.globalAlpha=0.25;
		    this.ctx.fillStyle = '#0E0D0D';             
		    this.ctx.fillRect(progressPoint-2, this.canvasPadding, this.canvasWidth+this.canvasPadding-2, this.canvasHeight+this.canvasPadding);
		    this.ctx.globalAlpha=1;		    
		  }
		}

		this.placeSkillImages = function(skills,callback) {
		  var self = this;

		  var skillImageSize = 46;

		  var images = [];
		  var loadedImages = 0;
		  var numImages = skills.skillDetails.length;

		  skills.skillDetails.forEach(function(skill,x){
		  	console.log("placing skill images", skill);
		  	//create the invisible div for each skill.
		  	var gridX = ((skill.loc[0] * 70) + ((70 - skillImageSize) / 2)) + self.canvasPadding;
		  	var gridY = ((skill.loc[1] * 70) + ((70 - skillImageSize) / 2)) + self.canvasPadding;
		  	console.log("with x and y", gridX, gridY);
		 	var div = $('<div>', {
			    class: 'skill-image',
                alt: 'skill,1,'+skills.skillMap[x],// skill name
                //TO DO determine if skill is available to add a point to or not??
			    style: 'top:' + gridY + 'px;left:' + gridX + 'px;width:'+skillImageSize+';height:'+skillImageSize+';'
		 	});
		 	
		 	$('#tree-overlay').append(div);
		 	//create the image
		 	var image = new Image();
			image.src = skill.image_selected;
			image.onload = function(){
				console.log("image onload", image, gridX, gridY, loadedImages, numImages);
				//draw the image
				self.ctx.drawImage(image,gridX,gridY);
				if(++loadedImages >= numImages){
					console.log("images are now loaded into the browser");
		  			callback();
				}	
			}
            
            // to do check if runes exist and draw them
            if(skill.runeMap.length > 0){
                skill.runeDetails.forEach(function(rune,y){
                    // todo fix rune image size and scaling
                    
                    var runeX = ((rune.loc[0] * 70) + ((70 - skillImageSize) / 2)) + self.canvasPadding;
                    var runeY = ((rune.loc[1] * 70) + ((70 - skillImageSize) / 2)) + self.canvasPadding;
                    
                    var div = $('<div>',{
                        class: 'skill-image',
                        alt: 'rune,1,'+skills.skillMap[x]+','+skill.runeMap[y],
                        style: 'top:' + runeY + 'px;left:' + runeX + 'px;width:'+skillImageSize+';height:'+skillImageSize+';'
                    });
                    $('#tree-overlay').append(div);
                    var image = new Image();
                        image.src = rune.image_selected;
                        image.onload = function(){
                            
                            //draw the image
                            self.ctx.drawImage(image,runeX,runeY);
                            // to do fix callback for runes + skills
                            /*
                            if(++loadedImages >= numImages){
                                console.log("images are now loaded into the browser");
                                //callback();
                            }	
                            */
                        }
                    
                })
            }
		  });
            
		}   

		this.redrawCanvas = function(progress,skills){
            var self = this;
            
           
            
			this.ctx.clearRect(0,0,this.canvasWidthPadded, this.canvasHeightPadded);
            $('#tree-overlay div').remove();
			this.drawTreeLines();           
            
			this.placeSkillImages(skills, function imagesLoaded(){
				self.drawProgressShadow(progress);
				self.drawProgressBar(progress);
				self.drawProgressMarkers(progress);
			});
		}
	}
    
	var Setup = function(){
		var self = this;
		this.injectHtml = function(){
			var template = '<div id="sidebar-wrap">\
			</div>\
			<div id="canvas-wrap">\
				<div id="tree-overlay">\
				</div>\
				<canvas id="tree-canvas">\
				</canvas>\
			</div>';
			app.ele.html(template);
		}
		this.setListeners = function(ele){
            var self = this;
            //spec selectors
            $(ele).on('click', '.spec-button', function(){
                console.log("clicked spec button");
                var clickedSpec = $(this);
                
                if(clickedSpec.hasClass('selected')){
                    return false;
                }
                
                var selectedSpec = $('.spec-button.selected');
                if(selectedSpec.length !== 0){
                    selectedSpec.removeClass('selected');
                }
                
                clickedSpec.toggleClass('selected');
                
                app.selectedSpec = clickedSpec.text();
                self.redrawAppCanvas();
            });
            
			$(ele).on('click', '.skill-image', function(event){
                var skill = $(this);
	    		
	    		console.log("clicked");
                
                if(app.getPointsRemaining() === 0){
                    return false;
                }
                
                var attributes = skill.attr('alt').split(',');
                var type = attributes[0];
                var available = attributes[1] > 0;
                var skillName = attributes[2];
                if(available){
                   // app.pointsAvailable -= 1;
                    if(type === 'skill'){
                      console.log("skill clicked");  
                    }
                    if(type === 'rune'){
                        var runeName = attributes[3];
                        console.log("rune clicked");                        
                    }
                }
			});
            
            
		}
		this.getSkillData = function(callback){
			$.getJSON( "./js/skillData.json", function ( data ) {
				if(Object.keys(data).length === 4){
					callback(null,data);
				} else {
					callback("Skill data does not match specification");
				}			
			}, function (err){
				callback(err);
			});
		}
		this.injectSidebar = function(){
			var $sidebar = $('#sidebar-wrap');				
			var specNames = app.skillData[app.selectedClass].builds;		
			if(!app.selectedSpec){				
				app.selectedSpec = specNames[0];
			}
			specNames.forEach(function(spec){	
				if(spec === app.selectedSpec){
					$sidebar.append($('<div class="spec-button selected">'+spec+'</div>'));	
				} else {		
					$sidebar.append($('<div class="spec-button">'+spec+'</div>'));	
				}
			});			
		}
		
		this.init = function(ele){
			console.log(ele);
			if(!ele.length){
				throw new Error("Please create a devilian-skill-planner div to init skill planner application");
			}
			app.ele = ele;		
			self.getSkillData(function(err,data){
				if(err){
					throw new Error("Unable to get skill data please retry." + err);
				}
				app.skillData = data;
				self.injectHtml();
				// to do must get a class before injecting sidebar.
				self.injectSidebar();

				app.canvas = new TreeCanvas;
                // to do get class to hand over skills.
				self.redrawAppCanvas();
				//set listeners to handle rest of application logic.
				self.setListeners(ele);
			});		
		}
        
        this.redrawAppCanvas = function(){
            return app.canvas.redrawCanvas(app.getCurrentSpecPoints(),app.skillData.berserker.skills[0]);
        }
	}

	return {
		init: new Setup().init
	}

})();

$(function(){
	SkillPlanner.init($('#devilian-skill-planner'));
});